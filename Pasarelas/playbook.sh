#!/bin/bash

#################################################################
# Se usa para lanzar los playbook de ansible
#
# Ejemplo: ./playbook.sh <playbook-ansible.yml> "hosts" -debug
#################################################################

if [[ $# -eq 2 ]] || [[ $# -eq 3 ]]; then
   if [[ $3 == "-debug" ]]; then export ANSIBLE_STDOUT_CALLBACK=skippy; fi
	  PLAYBOOK=$1
	  TARGET=$2
      QUISOC=`whoami`
      if [[ $QUISOC == "ibm-admin" ]]; then
         ansible-playbook $PLAYBOOK --extra-vars target=$TARGET
      else
         echo "Usuario incorrecto!!!" 
	     exit 1
      fi
else
    echo "NUMERO DE PARAMETROS INCORRECTOS"
fi